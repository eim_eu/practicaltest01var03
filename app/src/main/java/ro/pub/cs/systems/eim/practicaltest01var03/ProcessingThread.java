package ro.pub.cs.systems.eim.practicaltest01var03;

import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.util.Log;

import java.util.Date;
import java.util.Random;

public class ProcessingThread extends Thread
{
    private boolean isRunning;

    private int sum;
    private int subtraction;
    private Context context = null;
    private Random random = new Random();

    public ProcessingThread(Context context, int firstNumber, int secondNumber)
    {
        this.context = context;
        this.isRunning = true;

        sum = firstNumber + secondNumber;
        subtraction = firstNumber - secondNumber;
    }

    public static String[] actionTypes = {
            "ro.pub.cs.systems.eim.practicaltest01.sum",
            "ro.pub.cs.systems.eim.practicaltest01.subtraction"
    };
    private void sendMessage() {
        Intent intent = new Intent();
        intent.setAction(actionTypes[random.nextInt(actionTypes.length)]);
        intent.putExtra("BROADCAST_RECEIVER_EXTRA",
                new Date(System.currentTimeMillis()) + " " + sum + " " + subtraction);
        context.sendBroadcast(intent);
    }
    @Override
    public void run()
    {
        Log.d("PROCESSING_THREAD_TAG", "Thread has started! PID: " + Process.myPid() + " TID: " + Process.myTid());

        while (isRunning)
        {
            sendMessage();
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        Log.d("PROCESSING_THREAD_TAG", "Thread has stopped!");
    }

    public void stopThread()
    {
        this.isRunning = false;
    }
}
