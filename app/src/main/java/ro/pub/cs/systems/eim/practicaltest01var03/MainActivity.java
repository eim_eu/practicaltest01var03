package ro.pub.cs.systems.eim.practicaltest01var03;

import static ro.pub.cs.systems.eim.practicaltest01var03.ProcessingThread.actionTypes;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {
    Button plusButton, minusButton, navigateToSecondary;
    TextView display_text;
    EditText number1, number2;
    private int serviceStatus = 0;
    ActivityResultLauncher<Intent> activityResultLauncher;
    IntentFilter intentFilter = new IntentFilter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        plusButton = findViewById(R.id.plus_button);
        minusButton = findViewById(R.id.minus_button);
        navigateToSecondary = findViewById(R.id.navigate_button);
        display_text = findViewById(R.id.display_text);
        number1 = findViewById(R.id.number1);
        number2 = findViewById(R.id.number2);

        plusButton.setOnClickListener(it -> {

            int num1 = Integer.parseInt(number1.getText().toString());
            int num2 = Integer.parseInt(number2.getText().toString());
            display_text.setText(num1 + " + " + num2 + " = " + (num1 + num2));

            if(serviceStatus == 0) {
                Intent intent = new Intent(getApplicationContext(), MyService.class);
                intent.putExtra("service_text_key", display_text.getText().toString());
                getApplicationContext().startService(intent);
                serviceStatus = 1;
            }

        });

        minusButton.setOnClickListener(it -> {

            int num1 = Integer.parseInt(number1.getText().toString());
            int num2 = Integer.parseInt(number2.getText().toString());
            display_text.setText(num1 + " - " + num2 + " = " + (num1 - num2));

            if(serviceStatus == 0) {
                Intent intent = new Intent(getApplicationContext(), MyService.class);
                intent.putExtra("service_text_key", display_text.getText().toString());
                getApplicationContext().startService(intent);
                serviceStatus = 1;
            }
        });

        activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result.getResultCode() == RESULT_OK) {
                Toast.makeText(this, "Correct", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Incorrect", Toast.LENGTH_LONG).show();
            }
        });

        navigateToSecondary.setOnClickListener(it -> {
            Intent intent = new Intent(this, SecondaryActivity.class);
            intent.putExtra("result", display_text.getText().toString());
//            Log.d("andreea", "main: " + number1.getText().toString() + " " + number2.getText().toString());
            activityResultLauncher.launch(intent);
        });

        for (String actionType : actionTypes) {
            intentFilter.addAction(actionType);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("number1", number1.getText().toString());
        outState.putString("number2", number2.getText().toString());
        outState.putString("display_text", display_text.getText().toString());
        Toast.makeText(this, display_text.getText().toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("number1")) {
            number1.setText(savedInstanceState.getString("number1"));
        }
        if (savedInstanceState.containsKey("number2")) {
            number2.setText(savedInstanceState.getString("number2"));
        }
        if (savedInstanceState.containsKey("display_text")) {
            display_text.setText(savedInstanceState.getString("display_text"));
        }
    }

    BroadcastListener broadcastListener = new BroadcastListener();
    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(broadcastListener, intentFilter, Context.RECEIVER_EXPORTED);
        } else {
            registerReceiver(broadcastListener, intentFilter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(this, MyService.class);
        stopService(intent);
    }
}