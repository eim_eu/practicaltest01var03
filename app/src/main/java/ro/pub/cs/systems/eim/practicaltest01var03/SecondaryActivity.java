package ro.pub.cs.systems.eim.practicaltest01var03;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondaryActivity extends AppCompatActivity {
    Button correct, incorrect;
    TextView result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        correct = findViewById(R.id.correct_button);
        incorrect = findViewById(R.id.incorrect_button);
        result = findViewById(R.id.result_text);

        Intent intent = getIntent();
//        -------------------------------------------------------------------
        result.setText(intent.getStringExtra("result"));

        correct.setOnClickListener(it -> {
            setResult(RESULT_OK, null);
            finish();
        });

        incorrect.setOnClickListener(it -> {
            setResult(RESULT_CANCELED, null);
            finish();

        });
//        -------------------------------------------------------------------
    }
}
