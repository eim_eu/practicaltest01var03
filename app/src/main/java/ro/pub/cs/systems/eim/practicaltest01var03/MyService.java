package ro.pub.cs.systems.eim.practicaltest01var03;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    ProcessingThread processingThread;
    public MyService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String text = intent.getStringExtra("service_text_key");
        String[] nrs = text.split(" ");
        Log.d("SERVICIUuuuuuuuuuuuuuuuuuuuu", text);
        int nr1 = Integer.parseInt(nrs[0]);
        Log.d("nr1111111", String.valueOf(nr1));
        int nr2 = Integer.parseInt(nrs[2]);
        Log.d("nr22222", String.valueOf(nr2));

        processingThread = new ProcessingThread(getApplicationContext(), nr1, nr2);
        Log.d("SERVICIU", "In clasa onStartCommand");
        processingThread.start();
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        processingThread.stopThread();
    }
}